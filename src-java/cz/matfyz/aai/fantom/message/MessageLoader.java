/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.message;

import java.util.Properties;

import cz.matfyz.aai.fantom.game.Graph;
import cz.matfyz.aai.fantom.server.Client;
import cz.matfyz.aai.fantom.server.ProtocolException;

/**
 * A common interface for message loaders.
 */
public interface MessageLoader {
	/**
	 * Loads the message from raw data, for the given client and graph.
	 * 
	 * @param messageData the data of the message.
	 * @param client the client, for which the message is loaded.
	 * @param graph the game graph, to which the message is loaded.
	 * @return the parsed message.
	 * @throws ProtocolException if there is a problem with parsing the
	 * 					message.
	 */
	public Message loadMessage(Properties[] messageData, Client client, Graph graph) throws ProtocolException;
}
