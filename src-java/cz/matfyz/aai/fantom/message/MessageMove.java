/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.message;

import java.util.Properties;

import cz.matfyz.aai.fantom.game.Graph;
import cz.matfyz.aai.fantom.server.ProtocolException;

/**
 * Message sent by clients to server, when they report moves
 * of their actors.
 */
public class MessageMove extends MessageMoveBase {

	@Override
	public String getMessageType() {
		return PROPERTY_MESSAGE_TYPE_MOVE;
	}
	
	public MessageMove(ActorMove[] moves) {
		super(moves);
	}
	
	public MessageMove(Graph graph, Properties[] messageData) throws ProtocolException {
		super(graph, messageData);
	}
}
