/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

import cz.matfyz.aai.fantom.game.Graph;
import cz.matfyz.aai.fantom.game.GraphFormatException;

/**
 * The message from the server to the clients that tells them that
 * the game had started.
 */
public class MessageStart extends Message {
	/**
	 * The graph, for which the message is created.
	 */
	private Graph graph;
	
	/**
	 * The raw data of the message.
	 */
	private Properties[] messageData;
	
	/**
	 * Returns the game graph sent along with this message.
	 * @return the game graph sent along with this message.
	 */
	public Graph getGraph() throws GraphFormatException {
		if(this.graph == null) {
			this.graph = new Graph(messageData);
		}
		return this.graph;
	}
	
	/**
	 * Returns the raw data of the message.
	 * @return the raw data of the message.
	 */
	public Properties[] getMessageData() {
		return this.messageData;
	}
	
	@Override
	public String getMessageType() {
		return Message.PROPERTY_MESSAGE_TYPE_START;
	}
	
	/**
	 * Initializes the message data for the given graph data.
	 * 
	 * @param graphData the data of the graph, for which the message is initialized.
	 */
	protected void initializeMessage(Collection<Properties> graphData) {
		ArrayList<Properties> message = new ArrayList<Properties>();
		Properties header = new Properties();
		header.setProperty(PROPERTY_MESSAGE_TYPE, PROPERTY_MESSAGE_TYPE_START);
		
		message.add(header);
		message.addAll(graphData);
		
		this.messageData = message.toArray(new Properties[message.size()]);
	}

	@Override
	public Properties[] serialize() {
		if(messageData == null) {
			if(graph == null)
				throw new IllegalStateException("Not messageData neither graph are specified");
			initializeMessage(Arrays.asList(graph.serialize()));
		}
		return messageData;
	}

	/**
	 * Initializes a new instance of the message with the given graph.
	 * 
	 * @param graph the graph, for which the message is initialized.
	 */
	public MessageStart(Graph graph) {
		if(graph == null)
			throw new IllegalArgumentException("graph must not be null");
		this.graph = graph;
	}
	
	/**
	 * Initializes a new instance of the message with the given graph data.
	 * 
	 * @param mesageData serialized specification of the game graph.
	 */
	public MessageStart(Properties[] graphData) {
		initializeMessage(Arrays.asList(graphData));
	}
}
