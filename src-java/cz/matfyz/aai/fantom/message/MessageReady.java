/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.message;

import java.util.Properties;

import cz.matfyz.aai.fantom.server.ProtocolException;


/**
 * The initial message from the client to the server.
 */
public class MessageReady extends Message {

	/**
	 * The name of the property that contains the name of the client.
	 */
	public static final String PROPERTY_CLIENT_NAME = "name";

	/**
	 * The name of the property that contains the type of the client.
	 */
	public static final String PROPERTY_CLIENT_TYPE = "type";
	
	/**
	 * The value of the property {@link #PROPERTY_CLIENT_TYPE} that
	 * tells that the client controls the phantom.
	 */
	public static final String PROPERTY_CLIENT_TYPE_PHANTOM = "phantom";
	
	/**
	 * The value of the property {@link #PROPERTY_CLIENT_TYPE} that
	 * tells that the client controls the detectives.
	 */
	public static final String PROPERTY_CLIENT_TYPE_DETECTIVE = "detective";
	
	/**
	 * The name of the client.
	 */
	private String clientName;
	
	/**
	 * The type of the client.
	 */
	private ClientType clientType;
	
	/**
	 * Returns the name of the client.
	 * @return the name of the client.
	 */
	public String getClientName() {
		return this.clientName;
	}
	
	/**
	 * Returns the type of the client.
	 * @return the type of the client.
	 */
	public ClientType getClientType() {
		return this.clientType;
	}
	
	@Override
	public String getMessageType() {
		return Message.PROPERTY_MESSAGE_TYPE_READY;
	}
	
	/**
	 * Parses the message.
	 * 
	 * @param messageData the raw data of the message.
	 * @throws ProtocolException if there is a problem with parsing the message,
	 * 				i.e. it does not adhere to the protocol.
	 */
	protected void load(Properties[] messageData) throws ProtocolException {
		if(messageData == null || messageData.length == 0)
			throw new IllegalArgumentException("No message data were provided");
		
		Properties header = null;
		for(int i = 0; i < messageData.length; i++) {
			if(messageData[i] == null)
				throw new IllegalArgumentException("null value in message data");
			if(messageData[i].containsKey(PROPERTY_MESSAGE_TYPE)) {
				header = messageData[i];
				break;
			}
		}
		
		if(header == null)
			throw new ProtocolException("The message header was not found", null);
		
		this.clientName = header.getProperty(PROPERTY_CLIENT_NAME);
		if(this.clientName == null || this.clientName.isEmpty())
			throw new ProtocolException("The client name was not specified", null);
		
		String clientTypeStr = header.getProperty(PROPERTY_CLIENT_TYPE);
		if(clientTypeStr == null || clientTypeStr.isEmpty())
			throw new ProtocolException("The client type was not specified", null);
		
		if(clientTypeStr.equals(PROPERTY_CLIENT_TYPE_DETECTIVE))
			this.clientType = ClientType.DETECTIVE;
		else if(clientTypeStr.equals(PROPERTY_CLIENT_TYPE_PHANTOM))
			this.clientType = ClientType.PHANTOM;
		else
			throw new ProtocolException("Invalid client type: '" + clientTypeStr + "'", null);
	}

	@Override
	public Properties[] serialize() {
		Properties header = new Properties();
		header.setProperty(PROPERTY_MESSAGE_TYPE, PROPERTY_MESSAGE_TYPE_READY);
		header.setProperty(PROPERTY_CLIENT_NAME, getClientName());
		switch(getClientType()) {
		case DETECTIVE:
			header.setProperty(PROPERTY_CLIENT_TYPE, PROPERTY_CLIENT_TYPE_DETECTIVE);
			break;
		case PHANTOM:
			header.setProperty(PROPERTY_CLIENT_TYPE, PROPERTY_CLIENT_TYPE_PHANTOM);
			break;
		}
		
		return new Properties[] { header };
	}

	/**
	 * Initializes a new instance of the message with a given name and client type.
	 * 
	 * @param clientName the name of the client.
	 * @param clientType the type of the client.
	 */
	public MessageReady(String clientName, ClientType clientType) {
		if(clientName == null || clientName.isEmpty())
			throw new IllegalArgumentException("The client name was not specified");
		if(clientName.matches(".*[,:].*"))
			throw new IllegalArgumentException("The client name contains illegal characters");
		this.clientName = clientName;
		this.clientType = clientType;
	}
	
	public MessageReady(Properties[] message) throws ProtocolException {
		load(message);
	}
}
