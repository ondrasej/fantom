/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.game;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import cz.matfyz.aai.fantom.game.Graph.Node;
import cz.matfyz.aai.fantom.message.ClientType;
import cz.matfyz.aai.fantom.message.MessageMoveBase.ActorMove;
import cz.matfyz.aai.fantom.server.ProtocolException;

/**
 * A read-only wrapper over the actor objects. Allows the user to read
 * all properties, but not to change them in any way.
 */
public class ActorReadOnly extends Actor {
	/**
	 * The actor, over which this wrapper operates.
	 */
	private Actor sourceActor;

	@Override
	public void addTicket(TransportType transport) {
		throw new IllegalStateException("This actor is read-only");
	}

	@Override
	public void addTickets(TransportType transport, int count) {
		throw new IllegalStateException("This actor is read-only");	
	}

	@Override
	public boolean canMove(Node source, Node destination) {
		return sourceActor.canMove(source, destination);
	}

	@Override
	public boolean canMoveDoubleMove(Node source, Node destination) {
		return sourceActor.canMoveDoubleMove(source, destination);
	}

	@Override
	public boolean canMoveSingleEdge(Node source, Node destination,
			TransportType transport) {
		return sourceActor.canMoveSingleEdge(source, destination, transport);
	}

	@Override
	public boolean canMoveSingleEdge(Node source, Node destination) {
		return sourceActor.canMoveSingleEdge(source, destination);
	}

	@Override
	public boolean canMoveTo(Node destination, TransportType transport) {
		return sourceActor.canMoveTo(destination, transport);
	}

	@Override
	public boolean canMoveTo(Node destination) {
		return sourceActor.canMoveTo(destination);
	}

	@Override
	public void capture() {
		throw new IllegalStateException("This actor is read-only");
	}

	@Override
	public ClientType getClientType() {
		return sourceActor.getClientType();
	}

	@Override
	public Node getCurrentPosition() {
		return sourceActor.getCurrentPosition();
	}

	@Override
	public int getDoubleMoves() {
		return sourceActor.getDoubleMoves();
	}

	@Override
	public String getId() {
		return sourceActor.getId();
	}

	@Override
	public List<ActorMove> getLegalMoves(Graph graph) {
		List<ActorMove> moves = sourceActor.getLegalMoves(graph);
		for(int i = 0; i < moves.size(); i++) {
			ActorMove move = moves.get(i);
			assert move.getActor() == sourceActor;
			moves.set(i, new ActorMove(this, move.getTargetNode(), move.getTransportType()));
		}
		return moves;
	}

	@Override
	public int getNumberOfTickets(TransportType transport) {
		return sourceActor.getNumberOfTickets(transport);
	}

	@Override
	public Map<TransportType, Integer> getTickets() {
		return sourceActor.getTickets();
	}

	@Override
	public boolean hasDoubleMove() {
		return sourceActor.hasDoubleMove();
	}

	@Override
	public int hashCode() {
		return sourceActor.hashCode();
	}

	@Override
	public boolean hasTicket(TransportType transport1, TransportType transport2) {
		return sourceActor.hasTicket(transport1, transport2);
	}

	@Override
	public boolean hasTicket(TransportType transport) {
		return sourceActor.hasTicket(transport);
	}

	@Override
	public boolean hasTickets(TransportType transport1, int count1,
			TransportType transport2, int count2) {
		return sourceActor.hasTickets(transport1, count1, transport2, count2);
	}

	@Override
	public boolean hasTickets(TransportType transport, int count) {
		return sourceActor.hasTickets(transport, count);
	}

	@Override
	public boolean isCaptured() {
		return sourceActor.isCaptured();
	}

	@Override
	public boolean isDetective() {
		return sourceActor.isDetective();
	}

	@Override
	public boolean isHidden() {
		return sourceActor.isHidden();
	}

	@Override
	public boolean isPhantom() {
		return sourceActor.isPhantom();
	}

	@Override
	public PositionChange moveTo(Node target, TransportType transport)
			throws ProtocolException {
		throw new IllegalStateException("This actor is read-only");
	}

	@Override
	public Properties serialize() {
		return sourceActor.serialize();
	}

	@Override
	public void setCurrentPosition(Node position) {
		throw new IllegalStateException("This actor is read-only");
	}

	@Override
	public void setNumberOfTickets(TransportType transport, int count) {
		throw new IllegalStateException("This actor is read-only");
	}

	@Override
	public String toString() {
		return sourceActor.toString();
	}
	
	@Override
	public void undoMove(PositionChange change) {
		throw new IllegalStateException("This actor is read-only");
	}

	/**
	 * Initializes a new read-only wrapper for the given actor object.
	 * @param source the actor, for which the wrapper is created.
	 */
	public ActorReadOnly(Actor source) {
		if(source == null)
			throw new IllegalArgumentException("source must not be null");
		this.sourceActor = source;
	}
}
