/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.game;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * A single transport type used for edges in the game graph.
 */
public class TransportType {
	/**
	 * The name of the property that contains allowed users of the transport
	 * type.
	 */
	public static final String PROPERTY_USERS = "users";
	
	/**
	 * The value of the property {@see #PROPERTY_USERS} that specifies that
	 * anyone can use the transport type.
	 */
	public static final String PROPERTY_USERS_ALL = "all";
	
	/**
	 * The value of the property {@see #PROPERTY_USERS} that specifies that
	 * the the detectives can use the transport type.
	 */
	public static final String PROPERTY_USERS_DETECTIVE = "detective";
	
	/**
	 * The value of the property {@see #PROPERTY_USERS_PHANTOM} that specifies
	 * that the phantom can use the transport type.
	 */
	public static final String PROPERTY_USERS_PHANTOM = "phantom";
	
	/**
	 * The value of the bit mask {@see #users} that specifies that the
	 * detectives can use the transport type.
	 */
	public static final int USER_DETECTIVE = 1;
	
	/**
	 * The value of the bit mask {@see #users} that specifies that the
	 * phantom can use the transport type.
	 */
	public static final int USER_PHANTOM = 2;
	
	/**
	 * The value of the bit mask {@see #users} that specifies that anyone
	 * can use the transport type.
	 */
	public static final int USER_ANY = 3;
	
	/**
	 * The list of forbidden transport type names.
	 */
	public static final Set<String> FORBIDDEN_NAMES =
		Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(
				"actor", "detective", "double", "edge", "game", "message",
				"node", "nodes", "phantom", "tickets" 
		))); 
	
	/**
	 * The name of the transport type.
	 */
	private String name;
	
	/**
	 * The allowed users of the transport type.
	 */
	private int users;
	
	/**
	 * Returns the name of the transport type.
	 * @return the name of the transport type.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the allowed users of the transport type.
	 * @return the allowed users of the transport type.
	 */
	public int getUsers() {
		return users;
	}

	/**
	 * Return <code>true</code> if this transport can be used by detectives.
	 * @return <code>True</code> if this transport can be used by detectives.
	 */
	public boolean isUsedByDetective() {
		return 0 != (USER_DETECTIVE & getUsers());
	}
	
	/**
	 * Return <code>true</code> if this transport can be used by phantom.
	 * @return <code>True</code> if this transport can be used by phantom.
	 */
	public boolean isUsedByPhantom() {
		return 0 != (USER_PHANTOM & getUsers());
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object other) {
		TransportType o = (TransportType) other;
		return this.getName().equals(o.getName()) && this.getUsers() == o.getUsers();
	}
	
	/**
	 * Loads the properties of the transport type.
	 * @param properties the properties of the transport type.
	 */
	protected void loadProperties(Properties properties) throws GraphFormatException {
		String name = properties.getProperty(Graph.PROPERTY_TRANSPORT);
		if(name == null || name.isEmpty())
			throw new IllegalArgumentException("The name of the transport type is missing.");
		if(FORBIDDEN_NAMES.contains(name))
			throw new GraphFormatException("Forbidden transport type name: " + name);
		
		this.name = name;
		String users_str = properties.getProperty(PROPERTY_USERS);
		if(users_str == null)
			throw new IllegalArgumentException("The allowed users of the transport type were not specified");
		
		this.users = 0;
		if(users_str.contains(PROPERTY_USERS_DETECTIVE))
			this.users |= USER_DETECTIVE;
		if(users_str.contains(PROPERTY_USERS_PHANTOM))
			this.users |= USER_PHANTOM;
		if(users_str.contains(PROPERTY_USERS_ALL))
			this.users |= USER_ANY;
		
		if(this.users == 0)
			throw new IllegalArgumentException("No allowed users were specified for the transport type");
	}
	
	/**
	 * Serializes the transport type to the format, from which it was loaded.
	 * @return specification of the transport type.
	 */
	public Properties serialize() {
		Properties res = new Properties();
		res.setProperty(Graph.PROPERTY_TRANSPORT, getName());
		switch(getUsers()) {
		case USER_DETECTIVE:
			res.setProperty(PROPERTY_USERS, PROPERTY_USERS_DETECTIVE);
			break;
		case USER_PHANTOM:
			res.setProperty(PROPERTY_USERS, PROPERTY_USERS_PHANTOM);
			break;
		case USER_ANY:
			res.setProperty(PROPERTY_USERS, PROPERTY_USERS_ALL);
			break;
		}
		
		return res;
	}
	
	/**
	 * Creates a new transport type with the given name and allowed users.
	 * @param name the name of the transport type.
	 * @param users the allowed users of the transport type.
	 */
	public TransportType(String name, int users) throws GraphFormatException {
		if(name == null || name.isEmpty())
			throw new IllegalArgumentException("name must not be null or empty");
		if(FORBIDDEN_NAMES.contains(name))
			throw new GraphFormatException("Forbidden transport type name: " + name);
		this.name = name;
		this.users = users;
	}
	
	/**
	 * Creates a new transport type according to the given specification.
	 * @param properties the specification of the transport type.
	 */
	public TransportType(Properties properties) throws GraphFormatException {
		loadProperties(properties);
	}
}
