/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.game;

import cz.matfyz.aai.fantom.utils.ParserException;

/**
 * The exception that is thrown when the format of the game graph
 * is not valid.
 */
public class GraphFormatException extends ParserException {
	
	/**
	 * Creates a new instance of the exception with the given error
	 * message.
	 * @param message the error message.
	 */
	public GraphFormatException(String message) {
		super(message);
	}
	
	/**
	 * Creates a new instance of the exception with the given error
	 * message and line number.
	 * @param message the error message.
	 * @param lineNumber the number of the line, where the error occurred.
	 */
	public GraphFormatException(String message, int lineNumber) {
		super(message, lineNumber);
	}
}
