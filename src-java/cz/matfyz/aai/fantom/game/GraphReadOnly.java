/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import cz.matfyz.aai.fantom.message.ClientType;
import cz.matfyz.aai.fantom.message.MessageMoveBase;
import cz.matfyz.aai.fantom.server.Client;
import cz.matfyz.aai.fantom.server.ProtocolException;

public class GraphReadOnly extends Graph {
	
	private Graph sourceGraph;
	
	private Map<Actor, Actor> actorMapping;
	
	private List<Actor> actors;
	
	@Override
	public Collection<Actor> capturedPhantoms() {
		ArrayList<Actor> captured = new ArrayList<Actor>();
		for(Actor actor : sourceGraph.capturedPhantoms()) {
			captured.add(actorMapping.get(actor));
		}
		return captured;
	}

	@Override
	public Object clone() {
		return sourceGraph.clone();
	}

	@Override
	public Actor getActor(String actorId) {
		return actorMapping.get(sourceGraph.getActor(actorId));
	}

	@Override
	public List<Actor> getActors() {
		return this.actors;
	}

	@Override
	public List<Actor> getActors(ClientType type) {
		ArrayList<Actor> actors = new ArrayList<Actor>();
		for(Actor actor : sourceGraph.getActors(type)) {
			actors.add(actorMapping.get(actor));
		}
		return actors;
	}

	@Override
	public List<Edge> getEdges() {
		return sourceGraph.getEdges();
	}

	@Override
	public int getGameLength() {
		return sourceGraph.getGameLength();
	}

	@Override
	public Node getNode(String nodeId) {
		return sourceGraph.getNode(nodeId);
	}

	@Override
	public Map<String, Node> getNodes() {
		return sourceGraph.getNodes();
	}

	@Override
	public Set<Integer> getPhantomReveals() {
		return sourceGraph.getPhantomReveals();
	}

	@Override
	public TransportType getTransportType(String name) {
		return sourceGraph.getTransportType(name);
	}

	@Override
	public Map<String, TransportType> getTransportTypes() {
		return sourceGraph.getTransportTypes();
	}

	@Override
	public Collection<Actor> moveActors(List<Actor> actors,
			MessageMoveBase message) throws ProtocolException {
		throw new IllegalStateException("This graph is read-only");
	}

	@Override
	public void placeActors(MessageMoveBase message) throws ProtocolException {
		throw new IllegalStateException("This graph is read-only");	}

	@Override
	public Properties[] serialize() {
		return sourceGraph.serialize();
	}

	@Override
	public void updateActors(MessageMoveBase message) {
		throw new IllegalStateException("This graph is read-only");	}

	@Override
	public void verifyActorPlacement(MessageMoveBase message, Client client)
			throws ProtocolException {
		sourceGraph.verifyActorPlacement(message, client);
	}

	public GraphReadOnly(Graph source) {
		if(source == null)
			throw new IllegalArgumentException("source must not be null");
		this.sourceGraph = source;
		
		this.actorMapping = new HashMap<Actor, Actor>();
		this.actors = new ArrayList<Actor>();

		for(Actor actor : source.getActors()) {
			Actor clone = new ActorReadOnly(actor);
			actorMapping.put(actor, clone);
			actors.add(clone);
		}
	}
}
