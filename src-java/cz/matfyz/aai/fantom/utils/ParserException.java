/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.utils;

/**
 * The exception that is thrown when there is a problem when parsing
 * the serialized data.
 */
public class ParserException extends Exception {

	/**
	 * The line number, on which the error occurred. Contains <code>null</code>
	 * if the line number was not known.
	 */
	private Integer lineNumber;
	
	/**
	 * Returns the line number, on which the error occured. Returns <code>null</code>
	 * if no line number was specified.
	 * @return the number of the line, on which the error occurred.
	 */
	public Integer getLineNumber() {
		return this.lineNumber;
	}
	
	/**
	 * Creates a new instance of the exception with the given error
	 * message.
	 * @param message the error message.
	 */
	public ParserException(String message) {
		super(message);
	}
	
	/**
	 * Creates a new instance of the exception with the given error
	 * message and line number.
	 * @param message the error message.
	 * @param lineNumber the number of the line, where the error occurred.
	 */
	public ParserException(String message, int lineNumber) {
		super(message);
		this.lineNumber = lineNumber;
	}
	
	/**
	 * Creates a new instance of the exception with the given error
	 * message and line number.
	 * @param message the error message.
	 * @param innerException the exception that caused throwing of this exception.
	 * @param lineNumber the number of the line, where the error occurred.
	 */
	public ParserException(String message, Throwable innerException, int lineNumber) {
		super(message, innerException);
		this.lineNumber = lineNumber;
	}
}
