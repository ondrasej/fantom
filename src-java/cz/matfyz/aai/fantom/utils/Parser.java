/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Map.Entry;

/**
 * A parser for simple comma-separated format: each line contains a single record,
 * each record contains zero or more fields. The fields are specified in the format
 * "name: value", and they are separated by commas (and an arbitrary number of
 * whitespace)
 */
public class Parser {
	/**
	 * The name of the charset used in the graph files.
	 */
	public static final String DATA_CHARSET = "UTF-8";
	
	/**
	 * Loads all records from the given file.
	 * 
	 * @param file the file, from which the records are read.
	 * @return an array that contains all files read from the file.
	 * @throws IOException if there is a problem when reading the data.
	 * @throws ParserException if there is a problem in the data format.
	 */
	public static Properties[] parse(File file) throws IOException, ParserException {
		if(file == null)
			throw new IllegalArgumentException("file must not be null");
		
		FileInputStream istream = new FileInputStream(file);
		try {
			return parse(istream);
		}
		finally {
			if(istream != null)
				istream.close();
		}
	}
	
	/**
	 * Loads all records from the given input stream.
	 * 
	 * @param input the input stream, from which the records are read.
	 * @return an array that contains all files read from the file.
	 * @throws IOException if there is a problem when reading the data.
	 * @throws ParserException if there is a problem in the data format.
	 */
	public static Properties[] parse(InputStream input) throws IOException, ParserException {
		if(input == null)
			throw new IllegalArgumentException("input must not be null");
		
		InputStreamReader streamReader = new InputStreamReader(input, Charset.forName(DATA_CHARSET));
		BufferedReader reader = new BufferedReader(streamReader);
		
		ArrayList<Properties> res = new ArrayList<Properties>();
		int lineNumber = 0;
		for(String ln = reader.readLine(); ln != null; ln = reader.readLine(), lineNumber++) {
			ln = ln.trim();
			if(ln.isEmpty() || ln.startsWith("#"))
				continue;
			
			try {
				res.add(parseLine(ln));
			}
			catch(Exception e) {
				throw new ParserException(e.getMessage(), e, lineNumber);
			}
		}

		return res.toArray(new Properties[res.size()]);
	}
	
	/**
	 * Parses a single line, extracts all fields from the line.
	 * @param line the string, from which the fields are read.
	 * @return the list of values read from the file.
	 * @throws IllegalArgumentException if the format of the line is not valid
	 */
	public static Properties parseLine(String line) {
		if(line == null)
			throw new IllegalArgumentException("line must not be null");
		
		Properties res = new Properties();
		line = line.trim();
		if(line.isEmpty())
			return res;
		
		String[] items = line.split(",");
		for(String item : items) {
			String[] parts = item.split(":");
			if(parts.length != 2)
				throw new IllegalArgumentException("line has invalid format: " + line);
			
			String name = parts[0].trim();
			String value = parts[1].trim();
			
			res.setProperty(name, value);
		}
		
		return res;
	}
	
	/**
	 * Parses all records in the specified list of strings.
	 * 
	 * @param lines the records in string format.
	 * @return the list of parsed records.
	 */
	public static Properties[] parseLines(String[] lines) {
		if(lines == null)
			throw new IllegalArgumentException("lines must not be null");
		Properties[] properties = new Properties[lines.length];
		
		for(int i = 0; i < lines.length; i++) {
			properties[i] = parseLine(lines[i]);
		}
		
		return properties;
	}
	
	/**
	 * Converts <code>properties</code> to the same format, which can be parsed
	 * by {@link #parseLine(String)}. This method assumes that the keys and values
	 * in <code>properties</code> only contain valid characters, and it does not
	 * escape the strings.
	 * 
	 * @param properties the properties, that are encoded
	 * @return the string that contains the properties
	 */
	public static String encodeProperties(Properties properties) {
		if(properties == null)
			throw new IllegalArgumentException("properties must not be null");
		
		StringBuilder sb = new StringBuilder();
		for(Entry<Object, Object> item : properties.entrySet()) {
			if(sb.length() > 0)
				sb.append(',');
			sb.append(item.getKey());
			sb.append(':');
			sb.append(item.getValue());
		}
		return sb.toString();
	}
}
