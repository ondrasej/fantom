package cz.matfyz.aai.fantom.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import cz.matfyz.aai.fantom.game.Actor;
import cz.matfyz.aai.fantom.game.Graph;
import cz.matfyz.aai.fantom.game.Graph.Node;
import cz.matfyz.aai.fantom.message.ClientType;
import cz.matfyz.aai.fantom.message.MessageMoveBase.ActorMove;

/**
 * An iterator over all legal combinations of moves of agents of the same type.
 * Such a combination of atomic moves is called a "multimove".
 */
/*
 * Implementation note: With n movable actors, a multimove can be taken for an n-digit number.
 * If k_i is a number of legal moves of actor i and if m_i is a selected move of the actor i,
 * the corresponding ID = m_1 + k_1 * (m_2 + m2 * (...)). Using this mapping,
 * a multimove can be restored from an integer value.
 *
 * Further, it may happen that although an actor does have a legal move, it is never possible
 * to preform it (always blocked). For this situation, a no-move action is always considered as
 * one more legal move.
 *
 * After an integer ID is converted to a multimove, it needs to be checked for a violation of rules:
 * <ul>
 * <li>An actor must not move to an occupied position.</li>
 * <li>An actor must move, if a movement is possible.</li>
 * </ul>
 */
public class MultiMoves implements Iterator<ActorMove[]> {
	/** Current game state. */
	private Graph game;

	/** Type of the client for which the multimoves are generated. */
	private ClientType clientType;

	/** Actual actors, for which the moves are generated.
	 * May not contain all actors of the <code>clientType</code>,
	 * but must contain all actors which can legaly move.
	 */
	private List<Actor> actors;

	/** Legal moves of all actors in the <code>actors</code> list.
	 * Items in <code>actors</code> and <code>legalMoves</code> can be paired by their indexes.
	 */
	private List<List<ActorMove>> legalMoves;

	/** Maximal id of a multimove. */
	private long ceiling = 0;

	/** Current id of a multimove. */
	private long current = -1;

	/** Create an iterator.
	 * @param game The current state of the game.
	 * @param clientType Type of the client for whom the multimoves ought to be generated.
	 */
	public MultiMoves(Graph game, ClientType clientType) {
		this.game = game;
		this.clientType = clientType;
		this.actors = this.game.getActors(clientType);
		List<Actor> active = new ArrayList<Actor>(this.actors.size());
		this.legalMoves = new ArrayList<List<ActorMove>>(this.actors.size());
		for(Actor a : this.actors) {
			List<ActorMove> legal = a.getLegalMoves(this.game);
			//System.err.println(a.getId() + " has legal " + legal.size());
			if(legal.isEmpty()) {
				continue;
			}

			active.add(a);
			this.legalMoves.add(legal);

			if(this.ceiling == 0) { // The "+1" stands for no-move.
				this.ceiling = legal.size() + 1;
			} else {
				this.ceiling *= legal.size() + 1;
			}
		}
		this.actors = active;

		this.advance();
		//System.err.println("Ceiling: " + this.ceiling + " " + this.current);
	}

	/**
	 * Return true if there is some more multimove available.
	 * @return True if some more multimove can be obtained.
	 */
	@Override
	public boolean hasNext() {
		return this.current < this.ceiling;
	}

	/**
	 * Return the current multimove and shift to the next one.
	 * @return The current multimove.
	 */
	@Override
	public ActorMove[] next() {
		ActorMove[] result = currentMultiMove().toArray(new ActorMove[0]);

		this.advance();

		return result;
	}

	/**
	 * Not implemented.
	 * @throws UnsupportedOperationException Always.
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot prune multimoves.");
	}

	/**
	 * Converts the <code>current</code> ID to a multimove.
	 * @return The current multimove.
	 */
	private List<ActorMove> currentMultiMove() {
		List<ActorMove> result = new ArrayList<ActorMove>(this.actors.size());

		long k = this.current;
		for(List<ActorMove> legal : this.legalMoves) {
			int i = (int) (k % (legal.size() + 1));
			if(i < legal.size()) {
				result.add(legal.get(i));
			} else {
				; // a no-move
			}
			k /= legal.size() + 1;
		}

		return result;
	}

	/**
	 * Advances a cursor to the next legal multimove.
	 * If there are no more legal multimoves, the cursor reaches the <code>ceiling</code>.
	 */
	private void advance() {
		do {
			this.current++;
		} while(this.current < this.ceiling && !isAdmissible(currentMultiMove()));
	}

	/**
	 * Verifies that the given multimove does not conflict with rules.
	 * A multimove conflicts with rules either if some actor moves to
	 * an occupied location, or if some actor can move, but does not.
	 * @param multimove The checked multimove.
	 * @return True if the multimove does not violate the rules. False otherwise.
	 */
	private boolean isAdmissible(List<ActorMove> multimove) {
		Set<Node> occupied = new HashSet<Node>();

		for(Actor a : this.game.getActors(this.clientType)) {
			if(a.getCurrentPosition() != null) {
				occupied.add(a.getCurrentPosition());
			}
		}

		Set<Node> tmpOccupied = new HashSet<Node>(occupied);

		for(ActorMove m : multimove) {
			if(tmpOccupied.contains(m.getTargetNode()) && m.getTargetNode() != m.getActor().getCurrentPosition()) {
				//System.err.println(GreedyBase.toString(multimove.toArray(new ActorMove[0])) + " conflicts at " + m.getTargetNode());
				return false;
			} else {
				if(m.getActor().getCurrentPosition() != null) {
					tmpOccupied.remove(m.getActor().getCurrentPosition());
				}
				tmpOccupied.add(m.getTargetNode());
			}
		}

		tmpOccupied = new HashSet<Node>(occupied);
		int i = 0;
		for(Actor a : this.game.getActors(this.clientType)) {
			if(i < multimove.size() && a.equals(multimove.get(i).getActor())) {
				if(a.getCurrentPosition() != null) {
					tmpOccupied.remove(a.getCurrentPosition());
				}
				tmpOccupied.add(multimove.get(i).getTargetNode());
				i++;
			} else { // we suggest a no-move for an actor a
				List<ActorMove> legal = a.getLegalMoves(this.game);

				for(ActorMove m : legal) {
					if(!tmpOccupied.contains(m.getTargetNode())) {
						//System.err.println(GreedyBase.toString(multimove.toArray(new ActorMove[0])) + " lacks a move of " + a.getId());
						return false; // we suggest a no-move, but the actor can clearly move
					}
				}
			}
		}

		return true;
	}
}

