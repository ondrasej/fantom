/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.client;

import cz.matfyz.aai.fantom.game.Graph;
import cz.matfyz.aai.fantom.message.ClientType;
import cz.matfyz.aai.fantom.message.MessageMoveBase.ActorMove;

/**
 * A common interface for the Fantom agents.
 */
public interface Agent {

	/**
	 * Returns the type of the client.
	 * @return the type of the client.
	 */
	public ClientType getClientType();
	
	/**
	 * Returns the name of the agent. This should be a unique name,
	 * ideally following the Java naming convention.
	 * @return the name of the agent.
	 */
	public String getName();
	
	/**
	 * The method called when the game starts.
	 * 
	 * @param graph the game graph, on which the game will be played.
	 */
	public void start(Graph graph);
	
	/**
	 * The method that is called after the other player made its move.
	 * The parameters contain the information about the movement of the
	 * actors of the other player.
	 * 
	 * @param moves moves of the actors of the other player. Only contains
	 * 				information about moves in the last turn.
	 */
	public void update(ActorMove[] moves);
	
	/**
	 * The method that is called to determine the movements of the actors
	 * of this client during its turn.
	 * 
	 * @return the method must return movements for all its actors for
	 * 			the current turn.
	 */
	public ActorMove[] move();
	
	/**
	 * The method that is called to determine placement of the actors of
	 * this client in the beginning of the game.
	 * 
	 * @return movement objects for each actor of the client. The transport
	 * 			types in these objects must be set to <c>null</c>.
	 */
	public ActorMove[] place();
	
	/**
	 * The method that is called when one of the players won the game.
	 * 
	 * @param winner the type of the player that won the game.
	 */
	public void end(ClientType winner);
	
	/**
	 * The method that is called once, after the client process started.
	 * The client should perform all "global" initialization in this method. 
	 */
	public void initialize();
	
	/**
	 * The method that is called once, right before the client process
	 * terminates. The client should perform any necessary cleanup in this
	 * method.
	 */
	public void quit();
}
