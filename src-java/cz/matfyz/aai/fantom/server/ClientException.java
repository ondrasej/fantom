/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.server;

/**
 * The exception that is thrown when a client does something wrong.
 */
public class ClientException extends ServerException {
	/**
	 * The client that caused the exception. Can be <code>null</code>
	 * if the client object is not available (i.e. on the client side).
	 */
	private Client client;
	
	/**
	 * Returns the client object that caused the exception.
	 * @return the client object that caused the exception. May return
	 * 			<code>null</code>, so please, be careful.
	 */
	public Client getClient() {
		return client;
	}
	
	@Override
	public String getMessage() {
		return composeMessage(super.getMessage(), client);
	}



	/**
	 * Sets the client that caused the exception. Overwrites the
	 * previous client.
	 * @param client the client that caused the exception. 
	 */
	public void setClient(Client client) {
		this.client = client;
	}
	
	/**
	 * Initializes a new instance of the exception.
	 * 
	 * @param message the error message for the exception.
	 * @param client the client that caused the exception.
	 */
	public ClientException(String message, Client client) {
		super(message);
		this.client = client;
	}
	
	/**
	 * Initializes a new instance of the exception.
	 * 
	 * @param message the error message for the exception.
	 * @param innerException the inner exception.
	 * @param client the client that caused the exception.
	 */
	public ClientException(String message, Exception innerException, Client client) {
		super(message, innerException);
		this.client = client;
	}
	
	/**
	 * Composes the error message for this exception. Adds full client
	 * name to the error message provided by the caller.
	 * 
	 * @param message the error message for the exception.
	 * @param client the client that caused the exception.
	 * @return a new error message that contains the full name and client
	 * 			type of the client (if available).
	 */
	protected static String composeMessage(String message, Client client) {
		if(client == null)
			return message;
		return String.format("%s: %s", client.getFullClientName(), message);
	}
}
