package cz.matfyz.aai.fantom.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reads input from the given source and writes it to the output. Runs in
 * a separate thread, so that it is independent on the rest of the program.
 * Closes both the input and output streams, when there are no more data
 * in the input.
 */
public class OutputLogger extends Thread {
	/**
	 * The name of the logger used by this class.
	 */
	protected static final String LOGGER_NAME = "cz.matfyz.aai.fantom.server.OutputLogger";
	
	/**
	 * The logger used by this class.
	 */
	protected Logger logger = Logger.getLogger(LOGGER_NAME);
	
	/**
	 * The size of the buffer used by the logger.
	 */
	public static final int BUFFER_SIZE = 1024;
	
	/**
	 * The stream, from which the data are read.
	 */
	private InputStream input;
	
	/**
	 * The stream, to which the data are stored.
	 */
	private OutputStream output;
	
	@Override
	public void run() {
		try {
			byte[] buffer = new byte[BUFFER_SIZE];
			int size = input.read(buffer);
			while(size >= 0) {
				output.write(buffer, 0, size);
				size = input.read(buffer);
			}
			
			input.close();
			output.close();
		}
		catch(IOException err) {
			logger.log(Level.SEVERE, "Could not write the data", err);
		}
	}
	
	/**
	 * Creates a new output logger for the given input and output streams.
	 * 
	 * @param input the stream, from which the data are read.
	 * @param output the stream, to which the data are written.
	 */
	public OutputLogger(InputStream input, OutputStream output) {
		if(input == null)
			throw new IllegalArgumentException("input must not be null");
		if(output == null)
			throw new IllegalArgumentException("output must not be null");
		
		this.input = input;
		this.output = output;
	}
	
	/**
	 * Creates a new output logger for the given input stream and a file,
	 * to which the data are stored.
	 * 
	 * @param input the stream, from which the data are read.
	 * @param outputFile the file, to which the data are stored.
	 * @throws IOException if there is a problem with writing to the file.
	 */
	public OutputLogger(InputStream input, File outputFile) throws IOException {
		if(input == null)
			throw new IllegalArgumentException("input must not be null");
		if(outputFile == null)
			throw new IllegalArgumentException("output must not be null");
		
		this.input = input;
		this.output = new FileOutputStream(outputFile);
	}
}
