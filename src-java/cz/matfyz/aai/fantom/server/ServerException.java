/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.server;

/**
 * The exception that is thrown when the server encounters a problem. 
 */
public class ServerException extends Exception {
	
	/**
	 * Initializes a new instance of the exception with the given error message.
	 * 
	 * @param message the error message for the exception.
	 */
	public ServerException(String message) {
		super(message);
	}
	
	/**
	 * Initializes a new instance of the exception with a given error message and
	 * inner exception.
	 * 
	 * @param message the error message for the exception.
	 * @param innerException the inner exception.
	 */
	public ServerException(String message, Exception innerException) {
		super(message, innerException);
	}
}
