#!/bin/sh

DIR=`dirname "$0"`

java -cp "$DIR"/lib/log4j-1.2.16.jar:"$DIR"/bin cz.matfyz.aai.fantom.server.Server "$@"
