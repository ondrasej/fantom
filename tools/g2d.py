#!/usr/bin/env python
'''
Reads a map description from stdin, writes a corresponding Graphviz (.dot) format to stdout.
Example:
	g2d.py < map.graph | dot -T png -o map.png
'''

import sys

GAME = 'game'
NODE = 'node'
NODES = 'nodes'
EDGE = 'edge'
TRANSPORT = 'transport'
DETECTIVE = 'detective'
PHANTOM = 'phantom'

GAME_ROUNDS = 'rounds'
GAME_REVEAL = 'reveal'
NODES_FROM = 'from'
NODES_TO = 'to'
DIRECTED_EDGE = '-'
UNDIRECTED_EDGE = '='
TRANSPORT_COLOR = 'color'

class Graph:
	def __init__(self, input):
		self.is_directed = False
		self.nodes = list()
		self.transports = dict()
		self.edges = list()
		self.game = dict()
		self.detectives = list()
		self.phantoms = list()
		self.__fill(input)

	def __fill(self, input):
		for line in input:
			items = line.split(',')
			info = dict()

			for item in items:
				(name, value) = [x.strip() for x in item.split(':')]
				info[name] = value

			if GAME in info:
				rounds = info[GAME]
				reveal = [int(x) for x in info[GAME_REVEAL].split(' ')]
				self.game = { GAME_ROUNDS : int(rounds), GAME_REVEAL : reveal }
			elif TRANSPORT in info:
				self.transports[info[TRANSPORT]] = info
			elif DETECTIVE in info:
				self.detectives.append(info)
			elif PHANTOM in info:
				self.phantoms.append(info)
			elif NODE in info:
				self.nodes.append(info)
			elif NODES in info:
				start = int(info[NODES_FROM])
				end = 1 + int(info[NODES_TO])
				for i in xrange(start, end):
					node = str(i)
					self.nodes.append({ NODE : node })
			elif EDGE in info:
				descr = info[EDGE]
				(connection, transports) = descr.split('@')
				transports = transports.split('+')
				if DIRECTED_EDGE in connection:
					self.is_directed = True
					separator = DIRECTED_EDGE
				elif UNDIRECTED_EDGE in connection:
					separator = UNDIRECTED_EDGE
				else:
					print >>sys.stderr, 'Unknown edge separator:', info
					continue
				(source, target) = connection.split(separator)
				for transport in transports:
					self.edges.append( (source, target, transport, separator) )

	def __dump(self, text):
		sys.stdout.write(text)

	def dump_dot(self):
		self.__dump('digraph' if self.is_directed else 'graph')
		self.__dump(' map {\n')
		self.__dump('rankdir=LR;\n')
		self.__dump('nodesep=0.1;\n')
		self.__dump('ranksep=0.1;\n')

		for node in self.nodes:
			self.__dump(node[NODE])
			self.__dump(';\n')
		for (source, target, transport, direction) in self.edges:
			dumped = []
			if self.is_directed:
				dumped.append( (source, target, '->') )
				if direction == UNDIRECTED_EDGE:
					dumped.append( (target, source, '->') )
			else:
				dumped.append( (source, target, '--') )
			trans = self.transports[transport]
			for (src, trg, conn) in dumped:
				self.__dump(src)
				self.__dump(conn)
				self.__dump(trg)
				if TRANSPORT_COLOR in trans:
					self.__dump(' [color="')
					self.__dump(trans[TRANSPORT_COLOR])
					self.__dump('"]')
				self.__dump(';\n')

		self.__dump('}\n')

if __name__ == '__main__':
	Graph(sys.stdin).dump_dot()
