/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.utils;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.Properties;

import junit.framework.TestCase;

public class ParserTest extends TestCase {
	public static final String SINGLE_PROPERTY = "id: 12345";
	
	public static final String MULTIPLE_PROPERTIES = "id: 12345, type: car, x: 5.0, y: 6.0";
	
	public static final String MULTIPLE_WORDS = "id: 1, type: cable car ";
	
	public static final String COMMENTS_AND_EMPTY_LINES = "transport: 1, type: car\n" +
			"\n" +
			"    \t# some comment with whitespace\n" + 
			"# The previous line was empty, and this one has a comment on it\n" +
			"transport: 2, type: metro\n";
	
	public void testParseSingle() {
		Properties p = Parser.parseLine(SINGLE_PROPERTY);
		assertEquals(1, p.size());
		assertTrue(p.containsKey("id"));
		assertEquals("12345", p.getProperty("id"));
	}
	
	public void testParseMultiple() {
		Properties p = Parser.parseLine(MULTIPLE_PROPERTIES);
		assertEquals(4, p.size());
		assertTrue(p.containsKey("id"));
		assertTrue(p.containsKey("type"));
		assertTrue(p.containsKey("x"));
		assertTrue(p.containsKey("y"));
		assertEquals("12345", p.getProperty("id"));
		assertEquals("car", p.getProperty("type"));
		assertEquals("5.0", p.getProperty("x"));
		assertEquals("6.0", p.getProperty("y"));
	}
	
	public void testParseMultipleWords() {
		Properties p = Parser.parseLine(MULTIPLE_WORDS);
		assertEquals(2, p.size());
		assertTrue(p.containsKey("id"));
		assertTrue(p.containsKey("type"));
		assertEquals("1", p.getProperty("id"));
		assertEquals("cable car", p.getProperty("type"));
	}
	
	public void testParseCommentsAndEmptyLines() throws Exception {
		Charset utf8 = Charset.forName(Parser.DATA_CHARSET);
		byte[] data = utf8.encode(COMMENTS_AND_EMPTY_LINES).array();
		
		Properties[] p = Parser.parse(new ByteArrayInputStream(data));
		
		assertNotNull(p);
		assertEquals(2, p.length);
		
		assertNotNull(p[0]);
		assertEquals(2, p[0].size());
		assertEquals("1", p[0].getProperty("transport"));
		assertEquals("car", p[0].getProperty("type"));
		
		assertNotNull(p[1]);
		assertEquals(2, p[1].size());
		assertEquals("2", p[1].getProperty("transport"));
		assertEquals("metro", p[1].getProperty("type"));
	}
}
