package cz.matfyz.aai.fantom.game;

import java.util.List;

import cz.matfyz.aai.fantom.message.ClientType;
import cz.matfyz.aai.fantom.message.MessageMove;
import cz.matfyz.aai.fantom.message.MessageMoveBase.ActorMove;

import junit.framework.TestCase;

public class GraphReadOnlyTest extends TestCase {
	public static final String[] TEST_GRAPH = new String[] {
		"game: 5, reveal: 0 5",
		"nodes: , from: 1, to: 6",
		"transport: tram, users: all",
		"transport: bus, users: all",
		"edge: 1=2@tram",
		"edge: 2=3@bus",
		"edge: 3=4@tram",
		"edge: 4=5@bus",
		"edge: 5=6@tram",
		"edge: 6=1@bus",
		"phantom: Fantomas, tram: 1, bus: 1",
		"phantom: Joker, tram: 1, bus: 1",
		"detective: Poirot, tram: 10, bus: 10, universal: 5",
		"detective: Batman, tram: 10, bus: 10, universal: 5",
	};

	public void testEquality() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		GraphReadOnly gr = new GraphReadOnly(g);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor fantomasRO = gr.getActor("Fantomas");
		assertNotNull(fantomasRO);
		assertNotSame(fantomas, fantomasRO);
		assertEquals(fantomas, fantomasRO);
		
		List<Actor> actors = g.getActors();
		List<Actor> actorsRO = gr.getActors();
		assertEquals(actors.size(), actorsRO.size());
		for(int i = 0; i < actors.size(); i++) {
			assertEquals(actors.get(i), actorsRO.get(i));
			assertNotSame(actors.get(i), actorsRO.get(i));
		}

		actors = g.getActors(ClientType.DETECTIVE);
		actorsRO = gr.getActors(ClientType.DETECTIVE);
		assertEquals(actors.size(), actorsRO.size());
		for(int i = 0; i < actors.size(); i++) {
			assertEquals(actors.get(i), actorsRO.get(i));
			assertNotSame(actors.get(i), actorsRO.get(i));
		}

		actors = g.getActors(ClientType.PHANTOM);
		actorsRO = gr.getActors(ClientType.PHANTOM);
		assertEquals(actors.size(), actorsRO.size());
		for(int i = 0; i < actors.size(); i++) {
			assertEquals(actors.get(i), actorsRO.get(i));
			assertNotSame(actors.get(i), actorsRO.get(i));
		}
		
		g.placeActors(new MessageMove(new ActorMove[] {
				new ActorMove(g.getActor("Fantomas"), g.getNode("1"), null),
				new ActorMove(g.getActor("Joker"), g.getNode("2"), null),
		}));
		
		actors = g.getActors();
		actorsRO = gr.getActors();
		assertEquals(actors.size(), actorsRO.size());
		for(int i = 0; i < actors.size(); i++) {
			assertEquals(actors.get(i), actorsRO.get(i));
			assertNotSame(actors.get(i), actorsRO.get(i));
		}
		
		g.placeActors(new MessageMove(new ActorMove[] {
				new ActorMove(g.getActor("Poirot"), g.getNode("3"), null),
				new ActorMove(g.getActor("Batman"), g.getNode("4"), null),
		}));
	}
}
