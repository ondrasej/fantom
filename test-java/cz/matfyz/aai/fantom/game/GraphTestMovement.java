/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.game;

import java.util.List;

import cz.matfyz.aai.fantom.game.Graph.Node;
import cz.matfyz.aai.fantom.message.MessageMoveBase.ActorMove;
import cz.matfyz.aai.fantom.server.ProtocolException;
import junit.framework.TestCase;

public class GraphTestMovement extends TestCase {
	public static final String[] TEST_GRAPH = new String[] {
		"game: 5, reveal: 0 5",
		"nodes: , from: 1, to: 6",
		"transport: tram, users: all",
		"transport: bus, users: all",
		"edge: 1=2@tram",
		"edge: 2=3@bus",
		"edge: 3=4@tram",
		"edge: 4=5@bus",
		"edge: 5=6@tram",
		"edge: 6=1@bus",
		"phantom: Fantomas, tram: 1, bus: 1",
		"phantom: Joker, tram: 1, bus: 1",
		"detective: Poirot, tram: 10, bus: 10, universal: 5",
		"detective: Batman, tram: 10, bus: 10, universal: 5",
	};
	
	public void testCanMoveWithActors() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		
		Node n5 = g.getNode("5");
		assertNotNull(n5);
		
		Node n6 = g.getNode("6");
		assertNotNull(n6);
				
		// Check the number of tickets of Fantomas before doing setCurrentPosition
		assertEquals(2, fantomas.getTickets().size());
		assertEquals(0, fantomas.getDoubleMoves());
		assertEquals(1, fantomas.getNumberOfTickets(bus));
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		// Place Fantomas on the game graph
		assertNull(fantomas.getCurrentPosition());
		fantomas.setCurrentPosition(n1);
		assertSame(n1, fantomas.getCurrentPosition());
		// Check that Actor#setCurrentPosition did not change the numbers
		// of tickets of Fantomas
		assertEquals(2, fantomas.getTickets().size());
		assertEquals(0, fantomas.getDoubleMoves());
		assertEquals(1, fantomas.getNumberOfTickets(bus));
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		// Ok, fantomas can move to n2...
		assertTrue(fantomas.canMoveTo(n2));
		assertTrue(fantomas.canMoveTo(n2, tram));
		assertFalse(fantomas.canMoveTo(n2, bus));
		assertFalse(fantomas.canMoveTo(n2, universal));
		
		// Let's place a different phantom to n2...
		assertNull(joker.getCurrentPosition());
		joker.setCurrentPosition(n2);
		assertSame(n2, joker.getCurrentPosition());
		
		assertFalse(fantomas.canMoveTo(n2));
		assertFalse(fantomas.canMoveTo(n2, tram));
		assertTrue(fantomas.canMoveTo(n6));
		assertTrue(fantomas.canMoveTo(n6, bus));
		assertFalse(fantomas.canMoveTo(n6, tram));
		
		assertFalse(joker.canMoveTo(n1));
		assertFalse(joker.canMoveTo(n1, tram));
		assertFalse(joker.canMoveTo(n1, bus));
		assertFalse(joker.canMoveTo(n1, universal));
		
		// Let's place a detective to n6
		assertNull(poirot.getCurrentPosition());
		poirot.setCurrentPosition(n6);
		assertSame(n6, poirot.getCurrentPosition());
		
		assertTrue(poirot.canMoveTo(n1));
		assertTrue(poirot.canMoveTo(n1, bus));
		assertFalse(poirot.canMoveTo(n1, tram));
		assertTrue(poirot.canMoveTo(n1, universal));
		
		assertTrue(poirot.canMoveTo(n5));
		assertTrue(poirot.canMoveTo(n5, tram));
		assertFalse(poirot.canMoveTo(n5, bus));
		assertTrue(poirot.canMoveTo(n5, universal));
		
		// Now let's place a detective to n5
		assertNull(batman.getCurrentPosition());
		batman.setCurrentPosition(n5);
		assertSame(n5, batman.getCurrentPosition());
		
		assertFalse(batman.canMoveTo(n6));
		assertFalse(batman.canMoveTo(n6, bus));
		assertFalse(batman.canMoveTo(n6, tram));
		assertFalse(batman.canMoveTo(n6, universal));
		assertFalse(poirot.canMoveTo(n5));
		assertFalse(poirot.canMoveTo(n5, bus));
		assertFalse(poirot.canMoveTo(n5, tram));
		assertFalse(poirot.canMoveTo(n5, universal));
	}

	public void testMoveTickets() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n4 = g.getNode("4");
		assertNotNull(n4);
		
		fantomas.setCurrentPosition(n1);
		assertTrue(fantomas.canMoveTo(n2));
		
		assertEquals(2, fantomas.getTickets().size());
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(1, fantomas.getNumberOfTickets(bus));
		
		fantomas.moveTo(n2, tram);
		assertSame(n2, fantomas.getCurrentPosition());
		assertEquals(0, fantomas.getNumberOfTickets(tram));
		assertFalse(fantomas.canMoveTo(n1));
		assertFalse(fantomas.canMoveTo(n1, tram));
		
		try {
			// Try to move back to n1 (fantomas has no tram tickets now)...
			fantomas.moveTo(n1, tram);
			fail("Fantomas could move back to n1");
		}
		catch(ProtocolException e) {
			// OK, got the exception as expected
		}
	
		assertTrue(fantomas.canMoveTo(n3));
		assertTrue(fantomas.canMoveTo(n3, bus));
		assertFalse(fantomas.canMoveTo(n3, tram));
		assertFalse(fantomas.canMoveTo(n3, universal));
		
		try {
			// Try to move to n3 using a wrong transport type
			fantomas.moveTo(n3, tram);
		}
		catch(ProtocolException e) {
			// OK, got the exception as expected
		}
		
		fantomas.moveTo(n3, bus);
		assertFalse(fantomas.canMoveTo(n4));
		assertFalse(fantomas.canMoveTo(n2));
	}

	public void testLegalMoves() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor batman = g.getActor("Batman");
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n6 = g.getNode("6");
		assertNotNull(n6);
		
		fantomas.setCurrentPosition(n1);
		List<ActorMove> legalMoves = fantomas.getLegalMoves(g);
		
		assertNotNull(legalMoves);
		assertEquals(2, legalMoves.size());
		
		ActorMove move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n6) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(fantomas, move.getActor());
		assertSame(bus, move.getTransportType());
		
		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n2) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(fantomas, move.getActor());
		assertSame(tram, move.getTransportType());
		
		// Now place a different actor to node 6
		joker.setCurrentPosition(n6);
		
		// Check that legal moves for Fantomas did not change
		legalMoves = fantomas.getLegalMoves(g);
		assertNotNull(legalMoves);
		assertEquals(2, legalMoves.size());

		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n6) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(fantomas, move.getActor());
		assertSame(bus, move.getTransportType());
		
		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n2) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(fantomas, move.getActor());
		assertSame(tram, move.getTransportType());
		
		// Now place a detective to node 2
		batman.setCurrentPosition(n2);

		// Again, check that legal moves for Fantomas did not change
		legalMoves = fantomas.getLegalMoves(g);
		assertNotNull(legalMoves);
		assertEquals(2, legalMoves.size());

		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n6) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(fantomas, move.getActor());
		assertSame(bus, move.getTransportType());
		
		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n2) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(fantomas, move.getActor());
		assertSame(tram, move.getTransportType());
		
		legalMoves = batman.getLegalMoves(g);
		assertNotNull(legalMoves);
		assertEquals(4, legalMoves.size());
		
		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n1
					&& m.getTransportType() == tram) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(batman, move.getActor());

		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n1
					&& m.getTransportType() == universal) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(batman, move.getActor());

		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n3
					&& m.getTransportType() == bus) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(batman, move.getActor());

		move = null;
		for(ActorMove m : legalMoves) {
			assertNotNull(m);
			assertNotNull(m.getTargetNode());
			if(m.getTargetNode() == n3
					&& m.getTransportType() == universal) {
				move = m;
				break;
			}
		}
		assertNotNull(move);
		assertSame(batman, move.getActor());
	}
}
