/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.game;

import cz.matfyz.aai.fantom.game.Graph.Node;
import cz.matfyz.aai.fantom.message.ClientType;
import cz.matfyz.aai.fantom.message.MessageMove;
import cz.matfyz.aai.fantom.message.MessageMoveBase.ActorMove;
import cz.matfyz.aai.fantom.server.Client;
import cz.matfyz.aai.fantom.server.ProtocolException;

import junit.framework.TestCase;

public class GraphTestMessages extends TestCase {
	protected static final Client detectiveClient = new Client(new String[] { "foo" }, ClientType.DETECTIVE, "test", false);
	protected static final Client phantomClient = new Client(new String[] { "bar" }, ClientType.PHANTOM, "test", false);
	
	public static final String[] TEST_GRAPH = new String[] {
		"game: 5, reveal: 0 5",
		"nodes: , from: 1, to: 6",
		"transport: tram, users: all",
		"transport: bus, users: all",
		"edge: 1=2@tram",
		"edge: 2=3@bus",
		"edge: 3=4@tram",
		"edge: 4=5@bus",
		"edge: 5=6@tram",
		"edge: 6=1@bus",
		"phantom: Fantomas, tram: 1, bus: 1",
		"phantom: Joker, tram: 1, bus: 1",
		"detective: Poirot, tram: 10, bus: 10, universal: 5",
		"detective: Batman, tram: 10, bus: 10, universal: 5",
	};

	public void testPhantomPlacement() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		
		ActorMove[] moves = new ActorMove[] {
			new ActorMove(fantomas, n1, null),
			new ActorMove(joker, n2, null)
		};
		MessageMove msg = new MessageMove(moves);
		
		// First try to verify it as a message from the phantom client.
		// This should succeed, as the message contains a correct placement
		// of the phantoms.
		g.verifyActorPlacement(msg, phantomClient);
		
		// Then try to verify it as a message from the detective client. The
		// message places phantom actors, so this should fail.
		try {
			g.verifyActorPlacement(msg, detectiveClient);
			fail("Message validation failed - the detective client successfully placed phantom's actors");
		}
		catch(ProtocolException e) {
			// Ok, the message did not get through
		}
	}
	
	public void testPhantomPlacementInvalid() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		
		// Try a message, where a phantom actor is missing
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(fantomas, n1, null)
		};
		MessageMove msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, phantomClient);
			fail("Message validation failed - not all actors were placed");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where one actor is placed multiple times
		moves = new ActorMove[] {
				new ActorMove(fantomas, n1, null),
				new ActorMove(joker, n2, null),
				new ActorMove(fantomas, n3, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, phantomClient);
			fail("Message validation failed - an actor was placed multiple times");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where the transport type is specified for some
		// of the actors
		moves = new ActorMove[] {
				new ActorMove(fantomas, n1, tram),
				new ActorMove(joker, n2, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, phantomClient);
			fail("Message validation failed - transport type was specified for an actor");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where the target node is missing for some of
		// the actors.
		moves = new ActorMove[] {
				new ActorMove(fantomas, null, tram),
				new ActorMove(joker, n2, null),
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, phantomClient);
			fail("Message validation failed - target node was not specified for an actor");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where a detective actor is placed
		moves = new ActorMove[] {
				new ActorMove(fantomas, n1, null),
				new ActorMove(joker, n2, null),
				new ActorMove(batman, n3, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, phantomClient);
			fail("Message validation failed - an actor of the other player was placed");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where two actors are placed at the same node
		moves = new ActorMove[] {
				new ActorMove(fantomas, n1, null),
				new ActorMove(joker, n1, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, phantomClient);
			fail("Message validation failed - two actors were placed at the same position");
		}
		catch(ProtocolException e) {
		}
	}
	
	public void testDetectivePlacement() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(poirot, n1, null),
				new ActorMove(batman, n2, null)
		};
		MessageMove msg = new MessageMove(moves);
		
		// First try to verify it as a message from the detective client.
		// This should succeed, as the message contains a correct placement
		// of the detectives.
		g.verifyActorPlacement(msg, detectiveClient);
		
		// Then try to verify it as a message from the phantom client. The
		// message places detective actors, so this should fail.
		try {
			g.verifyActorPlacement(msg, phantomClient);
			fail("Message validation failed - the phantom client successfully placed detective's actors");
		}
		catch(ProtocolException e) {
			// Ok, the message did not get through
		}
	}
	
	public void testDetectivePlacementInvalid() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		
		// Try a message, where a phantom actor is missing
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(batman, n1, null)
		};
		MessageMove msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, detectiveClient);
			fail("Message validation failed - not all actors were placed");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where one actor is placed multiple times
		moves = new ActorMove[] {
				new ActorMove(batman, n1, null),
				new ActorMove(poirot, n2, null),
				new ActorMove(batman, n3, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, detectiveClient);
			fail("Message validation failed - an actor was placed multiple times");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where the transport type is specified for some
		// of the actors
		moves = new ActorMove[] {
				new ActorMove(batman, n1, tram),
				new ActorMove(poirot, n2, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, detectiveClient);
			fail("Message validation failed - transport type was specified for an actor");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where the target node is missing for some of
		// the actors.
		moves = new ActorMove[] {
				new ActorMove(batman, null, tram),
				new ActorMove(poirot, n2, null),
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, detectiveClient);
			fail("Message validation failed - target node was not specified for an actor");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where a detective actor is placed
		moves = new ActorMove[] {
				new ActorMove(poirot, n1, null),
				new ActorMove(joker, n2, null),
				new ActorMove(batman, n3, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, detectiveClient);
			fail("Message validation failed - an actor of the other player was placed");
		}
		catch(ProtocolException e) {
		}
		
		// Try a message, where two actors are placed at the same node
		moves = new ActorMove[] {
				new ActorMove(batman, n1, null),
				new ActorMove(poirot, n1, null)
		};
		msg = new MessageMove(moves);
		try {
			g.verifyActorPlacement(msg, detectiveClient);
			fail("Message validation failed - two actors were placed at the same position");
		}
		catch(ProtocolException e) {
		}
	}
	
	public void testPhantomMovementDifferentNodes() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n4 = g.getNode("4");
		assertNotNull(n4);
		Node n5 = g.getNode("5");
		assertNotNull(n5);
		Node n6 = g.getNode("6");
		assertNotNull(n6);
		
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(fantomas, n1, null),
				new ActorMove(joker, n3, null)
		};
		g.placeActors(new MessageMove(moves));
		moves = new ActorMove[] {
				new ActorMove(poirot, n2, null),
				new ActorMove(batman, n4, null)
		};
		g.placeActors(new MessageMove(moves));
		
		assertSame(n1, fantomas.getCurrentPosition());
		assertSame(n3, joker.getCurrentPosition());
		assertSame(n2, poirot.getCurrentPosition());
		assertSame(n4, batman.getCurrentPosition());
		
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(1, fantomas.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		assertEquals(1, joker.getNumberOfTickets(tram));
		assertEquals(1, joker.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		// Try to move the actors to different places
		moves = new ActorMove[] {
				new ActorMove(fantomas, n6, bus),
				new ActorMove(joker, n4, tram)
		};
		g.moveActors(g.getActors(ClientType.PHANTOM), new MessageMove(moves));
		
		assertSame(n6, fantomas.getCurrentPosition());
		assertSame(n4, joker.getCurrentPosition());
		
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(0, fantomas.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		assertEquals(0, joker.getNumberOfTickets(tram));
		assertEquals(1, joker.getNumberOfTickets(bus));
		assertEquals(0, joker.getNumberOfTickets(universal));
		
		// Try to move back to the previous position. This should fail,
		// as the phantoms do not have enough tickets
		moves = new ActorMove[] {
				new ActorMove(fantomas, n1, bus),
				new ActorMove(joker, n3, tram)
		};
		try {
			g.moveActors(g.getActors(ClientType.PHANTOM), new MessageMove(moves));
			fail("Movement validation failed - the actors do not have enough tickets");
		}
		catch(ProtocolException e) {
		}
	}
	
	public void testMovementInARow() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n4 = g.getNode("4");
		assertNotNull(n4);
		Node n5 = g.getNode("5");
		assertNotNull(n5);
		Node n6 = g.getNode("6");
		assertNotNull(n6);
		
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(fantomas, n1, null),
				new ActorMove(joker, n2, null)
		};
		g.placeActors(new MessageMove(moves));
		moves = new ActorMove[] {
				new ActorMove(poirot, n3, null),
				new ActorMove(batman, n4, null)
		};
		g.placeActors(new MessageMove(moves));
		
		assertSame(n1, fantomas.getCurrentPosition());
		assertSame(n2, joker.getCurrentPosition());
		assertSame(n3, poirot.getCurrentPosition());
		assertSame(n4, batman.getCurrentPosition());
		
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(1, fantomas.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		assertEquals(1, joker.getNumberOfTickets(tram));
		assertEquals(1, joker.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		moves = new ActorMove[] {
				new ActorMove(fantomas, n6, bus),
				new ActorMove(joker, n1, tram)
		};
		g.moveActors(g.getActors(ClientType.PHANTOM), new MessageMove(moves));
		
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(0, fantomas.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		assertEquals(0, joker.getNumberOfTickets(tram));
		assertEquals(1, joker.getNumberOfTickets(bus));
		assertEquals(0, joker.getNumberOfTickets(universal));
		
		moves = new ActorMove[] {
				new ActorMove(fantomas, n5, tram),
				new ActorMove(joker, n6, bus)
		};
		g.moveActors(g.getActors(ClientType.PHANTOM), new MessageMove(moves));
		
		assertEquals(0, fantomas.getNumberOfTickets(tram));
		assertEquals(0, fantomas.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		assertEquals(0, joker.getNumberOfTickets(tram));
		assertEquals(0, joker.getNumberOfTickets(bus));
		assertEquals(0, joker.getNumberOfTickets(universal));
	}
	
	public void testMovementInARowWrongOrder() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n4 = g.getNode("4");
		assertNotNull(n4);
		Node n5 = g.getNode("5");
		assertNotNull(n5);
		Node n6 = g.getNode("6");
		assertNotNull(n6);
		
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(fantomas, n2, null),
				new ActorMove(joker, n1, null)
		};
		g.placeActors(new MessageMove(moves));
		moves = new ActorMove[] {
				new ActorMove(poirot, n3, null),
				new ActorMove(batman, n4, null)
		};
		g.placeActors(new MessageMove(moves));
		
		assertSame(n2, fantomas.getCurrentPosition());
		assertSame(n1, joker.getCurrentPosition());
		assertSame(n3, poirot.getCurrentPosition());
		assertSame(n4, batman.getCurrentPosition());
		
		assertEquals(1, fantomas.getNumberOfTickets(tram));
		assertEquals(1, fantomas.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		assertEquals(1, joker.getNumberOfTickets(tram));
		assertEquals(1, joker.getNumberOfTickets(bus));
		assertEquals(0, fantomas.getNumberOfTickets(universal));
		
		moves = new ActorMove[] {
				new ActorMove(fantomas, n1, tram),
				new ActorMove(joker, n6, bus)
		};
		try {
			g.moveActors(g.getActors(ClientType.PHANTOM), new MessageMove(moves));
			fail("Movement message verification failed - phantoms moved in wrong order");
		}
		catch(ProtocolException e) {
		}
	}

	public static final String[] LARGER_GRAPH = new String[] {
		"game: 5, reveal: 0 5",
		"nodes: , from: 1, to: 6",
		"transport: tram, users: all",
		"transport: bus, users: all",
		"edge: 1=2@tram",
		"edge: 2=3@bus",
		"edge: 3=4@tram",
		"edge: 4=5@bus",
		"edge: 5=6@tram",
		"edge: 6=1@bus",
		"phantom: Fantomas, tram: 1, bus: 1",
		"phantom: Joker, tram: 1, bus: 1",
		"detective: Poirot, tram: 10, bus: 10, universal: 5",
		"detective: Batman, tram: 10, bus: 10, universal: 5",
		"detective: Japp, tram: 10, bus: 10, universal: 5"
	};

	public void testMovementImmobile() throws Exception {
		Graph g = new Graph(LARGER_GRAPH);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		Actor japp = g.getActor("Japp");
		assertNotNull(japp);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n4 = g.getNode("4");
		assertNotNull(n4);
		Node n5 = g.getNode("5");
		assertNotNull(n5);
		Node n6 = g.getNode("6");
		assertNotNull(n6);
		
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(fantomas, n2, null),
				new ActorMove(joker, n1, null)
		};
		g.placeActors(new MessageMove(moves));
		moves = new ActorMove[] {
				new ActorMove(poirot, n4, null),
				new ActorMove(batman, n3, null),
				new ActorMove(japp, n5, null),
		};
		g.placeActors(new MessageMove(moves));
		
		assertSame(n2, fantomas.getCurrentPosition());
		assertSame(n1, joker.getCurrentPosition());
		assertSame(n4, poirot.getCurrentPosition());
		assertSame(n3, batman.getCurrentPosition());
		assertSame(n5, japp.getCurrentPosition());
		
		moves = new ActorMove[] {
				new ActorMove(batman, n2, bus),
				new ActorMove(japp, n6, tram),
		};
		g.moveActors(g.getActors(ClientType.DETECTIVE), new MessageMove(moves));
		
		assertSame(n4, poirot.getCurrentPosition());
		assertSame(n2, batman.getCurrentPosition());
		assertSame(n6, japp.getCurrentPosition());
		
		moves = new ActorMove[] {
				new ActorMove(batman, n3, bus),
				new ActorMove(japp, n5, tram),
		};
		try {
			g.moveActors(g.getActors(ClientType.DETECTIVE), new MessageMove(moves));
			fail("Mesage verification failed - one of the detectives did not move");
		}
		catch(ProtocolException e) {			
		}
	}

	public void testMovementImmobileCannotMove() throws Exception {
		Graph g = new Graph(LARGER_GRAPH);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		TransportType universal = g.getTransportType("universal");
		assertNotNull(universal);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		Actor japp = g.getActor("Japp");
		assertNotNull(japp);
		
		Node n1 = g.getNode("1");
		assertNotNull(n1);
		Node n2 = g.getNode("2");
		assertNotNull(n2);
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n4 = g.getNode("4");
		assertNotNull(n4);
		Node n5 = g.getNode("5");
		assertNotNull(n5);
		Node n6 = g.getNode("6");
		assertNotNull(n6);
		
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(fantomas, n2, null),
				new ActorMove(joker, n1, null)
		};
		g.placeActors(new MessageMove(moves));
		moves = new ActorMove[] {
				new ActorMove(poirot, n4, null),
				new ActorMove(batman, n3, null),
				new ActorMove(japp, n5, null),
		};
		g.placeActors(new MessageMove(moves));
		
		assertSame(n2, fantomas.getCurrentPosition());
		assertSame(n1, joker.getCurrentPosition());
		assertSame(n4, poirot.getCurrentPosition());
		assertSame(n3, batman.getCurrentPosition());
		assertSame(n5, japp.getCurrentPosition());
		
		moves = new ActorMove[] {
				new ActorMove(poirot, n3, tram),
				new ActorMove(batman, n2, bus),
				new ActorMove(japp, n6, tram),
		};
		try {
			g.moveActors(g.getActors(ClientType.DETECTIVE), new MessageMove(moves));
			fail("Message validation failed - immobile detective did move");
		}
		catch(ProtocolException e) {
		}
	}
}
