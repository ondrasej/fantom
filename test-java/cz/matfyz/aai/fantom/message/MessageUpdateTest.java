/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.message;

import java.util.Properties;

import cz.matfyz.aai.fantom.game.Actor;
import cz.matfyz.aai.fantom.game.Graph;
import cz.matfyz.aai.fantom.game.TransportType;
import cz.matfyz.aai.fantom.game.Graph.Node;
import cz.matfyz.aai.fantom.message.MessageMoveBase.ActorMove;
import cz.matfyz.aai.fantom.message.MessageUpdate.ActorTickets;
import cz.matfyz.aai.fantom.server.ProtocolException;
import cz.matfyz.aai.fantom.utils.Parser;
import junit.framework.TestCase;

public class MessageUpdateTest extends TestCase {
	public static final String[] TEST_GRAPH = new String[] {
		"game: 5, reveal: 0 5",
		"nodes: , from: 1, to: 5",
		"transport: tram, users: all",
		"transport: bus, users: all",
		"edge: 1=2@tram",
		"edge: 2=3@tram",
		"edge: 3=4@tram",
		"edge: 4=5@tram",
		"phantom: Fantomas, tram: 1",
		"phantom: Joker, tram: 1",
		"detective: Poirot, tram: 10",
		"detective: Batman, tram: 10",
	};
	
	public void testCreateActorMoveMissingValues() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		
		try {
			new ActorMove(null, null, null);
			fail("ActorMove was successfully created with null values");
		}
		catch(IllegalArgumentException e) {
			// OK, let's go on
		}
		
		try {
			new ActorMove(fantomas, null, null);
			fail("ActorMove was successfully created with null target node and null transport type");
		}
		catch(IllegalArgumentException e) {
			// OK, let's go on
		}
		
		// Let's try if we can create ActorMoves with either just a transport
		// type, or a target node
		ActorMove move;
		move = new ActorMove(fantomas, g.getNode("1"), null);
		assertSame(fantomas, move.getActor());
		assertNotNull(move.getTargetNode());
		assertSame(g.getNode("1"), move.getTargetNode());
		assertNull(move.getTransportType());
		
		move = new ActorMove(fantomas, null, g.getTransportType("bus"));
		assertSame(fantomas, move.getActor());
		assertNull(move.getTargetNode());
		assertNotNull(move.getTransportType());
		assertSame(g.getTransportType("bus"), move.getTransportType());
		
		move = new ActorMove(fantomas, g.getNode("2"), g.getTransportType("tram"));
		assertSame(fantomas, move.getActor());
		assertNotNull(move.getTargetNode());
		assertSame(g.getNode("2"), move.getTargetNode());
		assertNotNull(move.getTransportType());
		assertSame(g.getTransportType("tram"), move.getTransportType());
	}
	
	public void testSerializeActorMove() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		
		Node nd1 = g.getNode("1");
		assertNotNull(nd1);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		
		ActorMove move = new ActorMove(fantomas, nd1, null);
		Properties record = move.serialize();
		
		assertNotNull(record);
		assertEquals(2, record.size());
		assertTrue(record.containsKey(MessageMoveBase.PROPERTY_ACTOR));
		assertEquals("Fantomas", record.getProperty(MessageMoveBase.PROPERTY_ACTOR));
		assertTrue(record.containsKey(MessageMoveBase.PROPERTY_TO_NODE));
		assertEquals("1", record.getProperty(MessageMoveBase.PROPERTY_TO_NODE));
		
		move = new ActorMove(fantomas, null, bus);
		record = move.serialize();
		
		assertNotNull(record);
		assertEquals(2, record.size());
		assertTrue(record.containsKey(MessageMoveBase.PROPERTY_ACTOR));
		assertEquals("Fantomas", record.getProperty(MessageMoveBase.PROPERTY_ACTOR));
		assertTrue(record.containsKey(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
		assertEquals("bus", record.getProperty(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
		
		move = new ActorMove(fantomas, nd1, bus);
		record = move.serialize();
		
		assertNotNull(record);
		assertEquals(3, record.size());
		assertTrue(record.containsKey(MessageMoveBase.PROPERTY_ACTOR));
		assertEquals("Fantomas", record.getProperty(MessageMoveBase.PROPERTY_ACTOR));
		assertTrue(record.containsKey(MessageMoveBase.PROPERTY_TO_NODE));
		assertEquals("1", record.getProperty(MessageMoveBase.PROPERTY_TO_NODE));
		assertTrue(record.containsKey(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
		assertEquals("bus", record.getProperty(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
	}
	
	public static final String ACTOR_MOVE_MISSING_DATA = "actor: Fantomas";
	public void testLoadInvalidActorMove() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		try {
			new ActorMove(g, Parser.parseLine(ACTOR_MOVE_MISSING_DATA));
			fail("Actor move was loaded without transport type and target node");
		}
		catch(ProtocolException e) {
			// OK, this is what should happen
		}
	}
	
	public static final String[] MESSAGE_UPDATE_PHANTOM = new String[] {
		"message: update",
		"actor: Fantomas, transport: tram",
		"actor: Joker, transport: tram",
		"tickets: Fantomas, tram: 10",
		"tickets: Poirot, tram: 10, double: 1",
		"tickets: Joker, tram: 2, double: 2",
		"tickets: Batman, tram: 2",
	};
	public void testLoadUpdatePhantom() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		MessageUpdate msg = new MessageUpdate(g, Parser.parseLines(MESSAGE_UPDATE_PHANTOM));

		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);

		ActorMove[] moves = msg.getMoves();
		assertNotNull(moves);
		assertEquals(2, moves.length);
		assertNotNull(moves[0].getActor());
		assertSame(g.getActor("Fantomas"), moves[0].getActor());
		assertNotNull(moves[0].getTransportType());
		assertSame(tram, moves[0].getTransportType());
		assertNull(moves[0].getTargetNode());
		
		assertNotNull(moves[1].getActor());
		assertSame(g.getActor("Joker"), moves[1].getActor());
		assertNotNull(moves[1].getTransportType());
		assertSame(tram, moves[1].getTransportType());
		assertNull(moves[1].getTargetNode());
		
		assertNotNull(msg.getCapturedPhantoms());
		assertEquals(0, msg.getCapturedPhantoms().size());
		
		ActorTickets[] tickets = msg.getActorTickets();
		assertNotNull(tickets);
		assertEquals(4, tickets.length);
		
		assertNotNull(tickets[0]);
		assertNotNull(tickets[0].getActor());
		assertSame(g.getActor("Fantomas"), tickets[0].getActor());
		assertNotNull(tickets[0].getTickets());
		assertEquals(1, tickets[0].getTickets().size());
		assertNotNull(tickets[0].getTickets().get(tram));
		assertEquals(10, tickets[0].getTickets().get(tram).intValue());
		assertEquals(0, tickets[0].getDoubleTickets());
		
		assertNotNull(tickets[1]);
		assertNotNull(tickets[1].getActor());
		assertSame(g.getActor("Poirot"), tickets[1].getActor());
		assertNotNull(tickets[1].getTickets());
		assertEquals(1, tickets[1].getTickets().size());
		assertNotNull(tickets[1].getTickets().get(tram));
		assertEquals(10, tickets[1].getTickets().get(tram).intValue());
		assertEquals(1, tickets[1].getDoubleTickets());
		
		assertNotNull(tickets[2]);
		assertNotNull(tickets[2].getActor());
		assertSame(g.getActor("Joker"), tickets[2].getActor());
		assertNotNull(tickets[2].getTickets());
		assertEquals(1, tickets[2].getTickets().size());
		assertNotNull(tickets[2].getTickets().get(tram));
		assertEquals(2, tickets[2].getTickets().get(tram).intValue());
		assertEquals(2, tickets[2].getDoubleTickets());
		
		assertNotNull(tickets[3]);
		assertNotNull(tickets[3].getActor());
		assertSame(g.getActor("Batman"), tickets[3].getActor());
		assertNotNull(tickets[3].getTickets());
		assertEquals(1, tickets[3].getTickets().size());
		assertNotNull(tickets[3].getTickets().get(tram));
		assertEquals(2, tickets[3].getTickets().get(tram).intValue());
		assertEquals(0, tickets[3].getDoubleTickets());
	}
	
	public static final String[] MESSAGE_PLACEMENT_DETECTIVE = new String[] {
		"message: update",
		"actor: Poirot, to: 1",
		"actor: Batman, to: 2",
		"tickets: Poirot, tram: 15",
		"tickets: Fantomas, tram: 15",
		"tickets: Joker, tram: 2",
		"tickets: Batman, tram: 3",
	};
	public void testLoadPlacementDetective() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		MessageUpdate msg = new MessageUpdate(g, Parser.parseLines(MESSAGE_PLACEMENT_DETECTIVE));
		
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		
		ActorMove[] moves = msg.getMoves();
		assertNotNull(moves);
		assertEquals(2, moves.length);
		
		assertNotNull(moves[0]);
		assertNotNull(moves[0].getActor());
		assertSame(g.getActor("Poirot"), moves[0].getActor());
		assertNotNull(moves[0].getTargetNode());
		assertSame(g.getNode("1"), moves[0].getTargetNode());
		assertNull(moves[0].getTransportType());
		
		assertNotNull(moves[1]);
		assertNotNull(moves[1].getActor());
		assertSame(g.getActor("Batman"), moves[1].getActor());
		assertNotNull(moves[1].getTargetNode());
		assertSame(g.getNode("2"), moves[1].getTargetNode());
		assertNull(moves[1].getTransportType());
		
		assertNotNull(moves[1]);
		
		assertNotNull(msg.getCapturedPhantoms());
		assertEquals(0, msg.getCapturedPhantoms().size());
	}
	
	public static final String[] MESSAGE_UPDATE_DETECTIVE = new String[] {
		"message: update",
		"actor: Poirot, to: 2, transport: tram",
		"actor: Batman, to: 3, transport: bus",
		"captured: Joker",
	};
	public void testLoadUpdateDetective() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		MessageUpdate msg = new MessageUpdate(g, Parser.parseLines(MESSAGE_UPDATE_DETECTIVE));
			
		ActorMove[] moves = msg.getMoves();
		assertNotNull(moves);
		assertEquals(2, moves.length);
		
		assertNotNull(moves[0]);
		assertNotNull(moves[0].getActor());
		assertSame(g.getActor("Poirot"), moves[0].getActor());
		assertNotNull(moves[0].getTargetNode());
		assertSame(g.getNode("2"), moves[0].getTargetNode());
		assertNotNull(moves[0].getTransportType());
		assertSame(g.getTransportType("tram"), moves[0].getTransportType());
		
		assertNotNull(moves[1]);
		assertNotNull(moves[1].getActor());
		assertSame(g.getActor("Batman"), moves[1].getActor());
		assertNotNull(moves[1].getTargetNode());
		assertSame(g.getNode("3"), moves[1].getTargetNode());
		assertNotNull(moves[1].getTransportType());
		assertSame(g.getTransportType("bus"), moves[1].getTransportType());
		
		assertNotNull(msg.getCapturedPhantoms());
		assertEquals(1, msg.getCapturedPhantoms().size());
		assertTrue(msg.getCapturedPhantoms().contains(g.getActor("Joker")));
	}
	
	public void testSerializeMessageUpdate() throws Exception {
		Graph g = new Graph(TEST_GRAPH);
		
		Actor fantomas = g.getActor("Fantomas");
		assertNotNull(fantomas);
		Actor poirot = g.getActor("Poirot");
		assertNotNull(poirot);
		Actor batman = g.getActor("Batman");
		assertNotNull(batman);
		Actor joker = g.getActor("Joker");
		assertNotNull(joker);
		
		TransportType bus = g.getTransportType("bus");
		assertNotNull(bus);
		TransportType tram = g.getTransportType("tram");
		assertNotNull(tram);
		
		Node n3 = g.getNode("3");
		assertNotNull(n3);
		Node n4 = g.getNode("4");
		assertNotNull(n4);
		
		ActorMove[] moves = new ActorMove[] {
				new ActorMove(fantomas, n3, null),
				new ActorMove(poirot, null, bus),
				new ActorMove(batman, n4, tram),
		};
		
		joker.capture();
		
		MessageUpdate msg = new MessageUpdate(g, moves, null);
		Properties[] msgData = msg.serialize();
		
		assertNotNull(msgData);
		assertEquals(9, msgData.length);
		
		assertNotNull(msgData[0]);
		assertEquals(1, msgData[0].size());
		assertTrue(msgData[0].containsKey(Message.PROPERTY_MESSAGE_TYPE));
		assertEquals(Message.PROPERTY_MESSAGE_TYPE_UPDATE, msgData[0].getProperty(Message.PROPERTY_MESSAGE_TYPE));
		
		assertNotNull(msgData[1]);
		assertEquals(2, msgData[1].size());
		assertTrue(msgData[1].containsKey(MessageMoveBase.PROPERTY_ACTOR));
		assertEquals("Fantomas", msgData[1].getProperty(MessageMoveBase.PROPERTY_ACTOR));
		assertTrue(msgData[1].containsKey(MessageMoveBase.PROPERTY_TO_NODE));
		assertEquals("3", msgData[1].getProperty(MessageMoveBase.PROPERTY_TO_NODE));
		
		assertNotNull(msgData[2]);
		assertEquals(2, msgData[2].size());
		assertTrue(msgData[2].containsKey(MessageMoveBase.PROPERTY_ACTOR));
		assertEquals("Poirot", msgData[2].getProperty(MessageMoveBase.PROPERTY_ACTOR));
		assertTrue(msgData[2].containsKey(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
		assertEquals("bus", msgData[2].getProperty(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
		
		assertNotNull(msgData[3]);
		assertEquals(3, msgData[3].size());
		assertTrue(msgData[3].containsKey(MessageMoveBase.PROPERTY_ACTOR));
		assertEquals("Batman", msgData[3].getProperty(MessageMoveBase.PROPERTY_ACTOR));
		assertTrue(msgData[3].containsKey(MessageMoveBase.PROPERTY_TO_NODE));
		assertEquals("4", msgData[3].getProperty(MessageMoveBase.PROPERTY_TO_NODE));
		assertTrue(msgData[3].containsKey(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
		assertEquals("tram", msgData[3].getProperty(MessageMoveBase.PROPERTY_TRANSPORT_TYPE));
		
		assertNotNull(msgData[4]);
		assertEquals(1, msgData[4].size());
		assertTrue(msgData[4].containsKey(MessageUpdate.PROPERTY_CAPTURED));
		assertEquals("Joker", msgData[4].getProperty(MessageUpdate.PROPERTY_CAPTURED));
		
		assertNotNull(msgData[5]);
		assertEquals(3, msgData[5].size());
		assertTrue(msgData[5].containsKey(MessageUpdate.PROPERTY_TICKETS));
		assertEquals("Fantomas", msgData[5].getProperty(MessageUpdate.PROPERTY_TICKETS));
		assertTrue(msgData[5].containsKey(Graph.PROPERTY_DOUBLE_TICKET));
		assertEquals("0", msgData[5].getProperty(Graph.PROPERTY_DOUBLE_TICKET));
		assertTrue(msgData[5].containsKey("tram"));
		assertEquals("1", msgData[5].getProperty("tram"));
		
		assertNotNull(msgData[6]);
		assertEquals(3, msgData[6].size());
		assertTrue(msgData[6].containsKey(MessageUpdate.PROPERTY_TICKETS));
		assertEquals("Joker", msgData[6].getProperty(MessageUpdate.PROPERTY_TICKETS));
		assertTrue(msgData[6].containsKey(Graph.PROPERTY_DOUBLE_TICKET));
		assertEquals("0", msgData[6].getProperty(Graph.PROPERTY_DOUBLE_TICKET));
		assertTrue(msgData[6].containsKey("tram"));
		assertEquals("1", msgData[6].getProperty("tram"));
	}
}
