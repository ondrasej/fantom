/*
 	This file is part of Fantom.

    Fantom is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Fantom is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Fantom.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.matfyz.aai.fantom.message;

import java.util.Properties;

import junit.framework.TestCase;

public class MessageReadyTest extends TestCase {
	
	public static final String TEST_CLIENT_NAME = "Just a client";

	protected Properties createSimpleHeader() {
		Properties header = new Properties();
		header.setProperty(Message.PROPERTY_MESSAGE_TYPE, Message.PROPERTY_MESSAGE_TYPE_READY);
		header.setProperty(MessageReady.PROPERTY_CLIENT_NAME, TEST_CLIENT_NAME);
		header.setProperty(MessageReady.PROPERTY_CLIENT_TYPE, MessageReady.PROPERTY_CLIENT_TYPE_DETECTIVE);
		return header;
	}
	
	public void testLoadSimple() throws Exception {
		MessageReady msgReady = new MessageReady(new Properties[] { createSimpleHeader() });
		
		assertEquals(TEST_CLIENT_NAME, msgReady.getClientName());
		assertEquals(ClientType.DETECTIVE, msgReady.getClientType());
	}
	
	public void testLoadMultipleRecords() throws Exception {
		Properties[] messageData = new Properties[] { 
										new Properties(), 
										new Properties(), 
										new Properties(),
										createSimpleHeader() };
		MessageReady msgReady = new MessageReady(messageData);

		assertEquals(msgReady.getClientName(), TEST_CLIENT_NAME);
		assertEquals(msgReady.getClientType(), ClientType.DETECTIVE);
	}
	
	public void createFromNameAndType() {
		MessageReady msgPhantom = new MessageReady(TEST_CLIENT_NAME, ClientType.PHANTOM);
		assertEquals(TEST_CLIENT_NAME, msgPhantom.getClientName());
		assertEquals(ClientType.PHANTOM, msgPhantom.getClientType());
		
		MessageReady msgDetective = new MessageReady(TEST_CLIENT_NAME, ClientType.DETECTIVE);
		assertEquals(TEST_CLIENT_NAME, msgDetective.getClientName());
		assertEquals(ClientType.DETECTIVE, msgDetective.getClientType());
	}
	
	public void testEmptyClientName() throws Exception {
		try {
			@SuppressWarnings("all")
			MessageReady msgReady = new MessageReady("", ClientType.DETECTIVE);
		}
		catch(IllegalArgumentException e) {
			return;
		}
		fail("The message was created with empty client name");
	}
	
	public void testInvalidClientName() throws Exception {
		try {
			@SuppressWarnings("all")
			MessageReady msgReady = new MessageReady("Hello, world", ClientType.DETECTIVE);
		}
		catch(IllegalArgumentException e) {
			return;
		}
		fail("The message was created with an invalid client name");
	}
	
	public void testSerializeDetective() throws Exception {
		MessageReady msg = new MessageReady(TEST_CLIENT_NAME, ClientType.DETECTIVE);
		Properties[] props = msg.serialize();
		
		assertNotNull(props);
		assertEquals(1, props.length);
		assertNotNull(props[0]);
		
		assertTrue(props[0].containsKey(Message.PROPERTY_MESSAGE_TYPE));
		assertEquals(Message.PROPERTY_MESSAGE_TYPE_READY,
				props[0].getProperty(Message.PROPERTY_MESSAGE_TYPE));
		
		assertTrue(props[0].containsKey(MessageReady.PROPERTY_CLIENT_NAME));
		assertEquals(TEST_CLIENT_NAME, props[0].getProperty(MessageReady.PROPERTY_CLIENT_NAME));
		
		assertTrue(props[0].containsKey(MessageReady.PROPERTY_CLIENT_TYPE));
		assertEquals(MessageReady.PROPERTY_CLIENT_TYPE_DETECTIVE,
				props[0].getProperty(MessageReady.PROPERTY_CLIENT_TYPE));
	}
	
	public void testSerializePhantom() throws Exception {
		MessageReady msg = new MessageReady(TEST_CLIENT_NAME, ClientType.PHANTOM);
		Properties[] props = msg.serialize();
		
		assertNotNull(props);
		assertEquals(1, props.length);
		assertNotNull(props[0]);
		
		assertTrue(props[0].containsKey(Message.PROPERTY_MESSAGE_TYPE));
		assertEquals(Message.PROPERTY_MESSAGE_TYPE_READY,
				props[0].getProperty(Message.PROPERTY_MESSAGE_TYPE));
		
		assertTrue(props[0].containsKey(MessageReady.PROPERTY_CLIENT_NAME));
		assertEquals(TEST_CLIENT_NAME, props[0].getProperty(MessageReady.PROPERTY_CLIENT_NAME));
		
		assertTrue(props[0].containsKey(MessageReady.PROPERTY_CLIENT_TYPE));
		assertEquals(MessageReady.PROPERTY_CLIENT_TYPE_PHANTOM,
				props[0].getProperty(MessageReady.PROPERTY_CLIENT_TYPE));
	}
}
