#!/bin/sh

graph="$1"
count="$2"
name="$3"

shift 3

./run.sh "$graph" "$count" "$name" java -cp bin cz.matfyz.aai.fantom.clients.RandomAgent PHANTOM ";" "$@" | tee "$name".log
